<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.90 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.12 -->
# kivy_help 0.3.40 - OUTDATED - merged into [new ae.kivy package](https://gitlab.com/ae-group/ae_kivy)

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_help/develop?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_help)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_help/release0.3.39?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_help/-/tree/release0.3.39)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_kivy_help)](
    https://pypi.org/project/ae-kivy-help/#history)

>ae namespace package portion kivy_help: enhance your app with context help, user onboarding, product tours, walkthroughs and tutorials.

[![Coverage](https://ae-group.gitlab.io/ae_kivy_help/coverage.svg)](
    https://ae-group.gitlab.io/ae_kivy_help/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_kivy_help/mypy.svg)](
    https://ae-group.gitlab.io/ae_kivy_help/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_kivy_help/pylint.svg)](
    https://ae-group.gitlab.io/ae_kivy_help/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_kivy_help)](
    https://gitlab.com/ae-group/ae_kivy_help/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_kivy_help)](
    https://gitlab.com/ae-group/ae_kivy_help/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_kivy_help)](
    https://gitlab.com/ae-group/ae_kivy_help/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_kivy_help)](
    https://pypi.org/project/ae-kivy-help/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_kivy_help)](
    https://gitlab.com/ae-group/ae_kivy_help/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_kivy_help)](
    https://libraries.io/pypi/ae-kivy-help)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_kivy_help)](
    https://pypi.org/project/ae-kivy-help/#files)


## installation


execute the following command to install the
ae.kivy_help package
in the currently active virtual environment:
 
```shell script
pip install ae-kivy-help
```

if you want to contribute to this portion then first fork
[the ae_kivy_help repository at GitLab](
https://gitlab.com/ae-group/ae_kivy_help "ae.kivy_help code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_kivy_help):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_kivy_help/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.kivy_help.html
"ae_kivy_help documentation").
